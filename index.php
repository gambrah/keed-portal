<!--<!doctype html>-->
<!--<html>-->
<!--<head>-->
<!--<meta charset="UTF-8">-->
<!--<title>Keed-NLA Login Portal</title>-->
<!--<link rel="stylesheet" type="text/css" href="_pros/portal-ui.css">-->
<!--<meta name="viewport" content="width=device-width, initial-scale=1.0">-->
<!--</head>-->
<!---->
<!--<body><div class="portal-header"><img src="_pros/images/nla-game.png" width="436" height="315"  alt=""/></div>-->
<!--<div class="portal">-->
<!--  <div class="portal-flux"><label>User Name</label><input type="text" placeholder="User Name">-->
<!--<p><label>Password</label><input type="password" placeholder="Password"></p><p class="button"><input type="text" value="Submit" class="submit-btn"></p></div>-->
<!--</div>-->
<!--<p class="own">©  Copyright --><?php //echo date("Y"); ?><!-- Keed-NLA (Portal)</p>-->
<!--</body>-->
<!--</html>-->
<!---->

<?php
session_start();

if (!isset($_SESSION['username'])) {
    $_SESSION['msg'] = "You must log in first";
    header('location: login.php');
}
if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['username']);
    header("location: login.php");
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Home</title>
    <link rel="stylesheet" type="text/css" href="_pros/portal-ui.css">
</head>
<body>


    <div class="topnav">
        <a href="index.php?logout='1'" class="active"  style="margin-right: 20px">Log Out</a>
    </div>
    <!-- notification message -->
    <?php
    session_start();
    ?>

    <!-- logged in user information -->
    <?php  if (isset($_SESSION['username'])) : ?>
        <div class="portal">
            <?php if (isset($_SESSION['number-sent'])) : ?>
                <div class="success" >
                    <h3>
                        <?php
                        echo $_SESSION['number-sent'];
                        unset($_SESSION['number-sent'])
                        ?>
                    </h3>
                </div>
            <?php endif ?>

            <p><?php $_SESSION['username']?></p>
            <form action="sendmail.php" method="post" enctype = "multipart/form-data">

                <div class="portal-flux"><label>Mobile</label>
                    <input type="text" placeholder="Enter mobile number" name="mobile">
                    <div class="or-one" style="margin-top: 10px;margin-bottom: 10px; text-align: center">
                                OR
                    </div>
                    <label>Upload CSV file</label>
                    <input type="file" name="csv_file" id="fileToUpload">
                    <button type="submit" class="submit-btn logout" name="login_user">Submit</button>
                </div>
            </form>
            <div class="signup">
                <p> <a href="index.php?logout='1'">logout</a> </p>
            </div>

        </div>
    <?php endif ?>

</body>
</html>

