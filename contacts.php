<?php

if(isset($_POST['full_name'])){
	include_once 'email.php';

	$message = '<p>You have a new contact, see details below</p><br>
	<p>Name: '.$_POST['full_name'].'</p>
	<p>Email: '.$_POST['email'].'</p>
	<p>Telephone: '.$_POST['telephone'].'</p>
	<p>Location: '.$_POST['location'].'</p>
	<p>Message:<br><br> '.$_POST['message'].'</p>';

	sendMessage($_POST['full_name'].' <'.$_POST['email'].'>', "what", 'New Contact - OMA', $message);

	$notification = "<div class=\"notification\">Thank you for writing to Open Mind Africa, We'll get back to you within 48 hours.</div>";
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<body>
<div class="form-data">
    <form method="post">
        <div><input type="text" required="required" name="full_name" placeholder="Full Name" /></div>
        <div><input type="text" required="required" name="email" placeholder="Email Address" /></div>
        <div><input type="text" required="required" name="telephone" placeholder="Telephone" /></div>
        <div><input type="text" required="required" name="location" placeholder="Location" /></div>
        <div><textarea name="message" required="required" placeholder="Leave a Message"></textarea></div>
        <div><input type="submit" value="Send" class="submit"></div>
    </form>
</div>

</body>
</html>
