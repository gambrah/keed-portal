<?php include('server.php') ?>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Keed-NLA Login Portal</title>
    <link rel="stylesheet" type="text/css" href="_pros/portal-ui.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body><div class="portal-header"><img src="_pros/images/nla-game.png" width="436" height="315"  alt=""/></div>
<div class="portal">
    <form action="login.php" method="post">
        <?php include('errors.php'); ?>

        <div class="portal-flux"><label>User Name</label><input type="text" placeholder="User Name" name="username">
            <p><label>Password</label><input type="password" placeholder="Password" name="password"></p>
            <button type="submit" class="submit-btn" name="login_user">Login</button>
        </div>
    </form>
    <div class="signup">
      <a href="register.php">Sign up</a>
    </div>

</div>
<p class="own">©  Copyright <?php echo date("Y"); ?> Keed-NLA (Portal)</p>
</body>
</html>